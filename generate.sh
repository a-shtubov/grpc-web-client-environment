#!/bin/bash 
SRC=./src

find $SRC/axxonsoft -type f -name '*.js' -delete
find $SRC/axxonsoft -type f -name '*.ts' -delete
find $SRC/google -type f -name '*.js' -delete
find $SRC/google -type f -name '*.ts' -delete

for FILE in $(find ./src -type f -name '*.proto'); do
  STRIPPED=$(echo $FILE | sed 's|^./src/||')
  protoc -I=$SRC $STRIPPED \
  --js_out=import_style=commonjs:$SRC \
  --grpc-web_out=import_style=commonjs+dts,mode=grpcwebtext:$SRC
done
