require("dotenv").config();
const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const [AUTH_LOGIN, AUTH_PASSWORD] = process.env.AUTH.split(":");
const API_PREFIX = "/grpc-web";

module.exports = {
  entry: path.resolve(__dirname, "./src/client.js"),
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "bundle.js",
  },
  plugins: [
    new HtmlWebpackPlugin(),
    new webpack.DefinePlugin({
      "process.env.AUTH_LOGIN": JSON.stringify(AUTH_LOGIN),
      "process.env.AUTH_PASSWORD": JSON.stringify(AUTH_PASSWORD),
      "process.env.API_PREFIX": JSON.stringify(API_PREFIX),
    }),
  ],
  devServer: {
    proxy: [
      {
        context: API_PREFIX,
        target: process.env.PROXY_TARGET,
        pathRewrite: { [`^${API_PREFIX}`]: "" },
      },
    ],
  },
};
