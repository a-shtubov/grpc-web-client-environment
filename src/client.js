import {
  ListCamerasRequest,
  ListCamerasResponse,
} from "./axxonsoft/bl/domain/Domain_pb";
import { DomainServicePromiseClient } from "./axxonsoft/bl/domain/Domain_grpc_web_pb";
import { API_PREFIX } from "./constants";
import { getAuthMetadata } from "./auth";

(async function main() {
  const authMetadata = await getAuthMetadata();
  const domainService = new DomainServicePromiseClient(API_PREFIX);
  const cameraRequest = new ListCamerasRequest();
  const stream = domainService.listCameras(cameraRequest, authMetadata);

  stream.on("data", (response) =>
    console.log(response.getItemsList().map((c) => c.toObject()))
  );

  stream.on("status", function (status) {
    console.log(status);
  });

  stream.on("end", function (end) {
    console.log("END");
  });
})();
