syntax = "proto3";
package axxonsoft.bl.archive;

option go_package = "bitbucket.org/Axxonsoft/bl/archive";

import "google/api/annotations.proto";
import "axxonsoft/bl/primitive/Primitives.proto";
import "axxonsoft/bl/media/Media.proto";

message ChangeBookmarksRequest
{
    message BookmarkInternal
    {
        string node_name = 3;
        bool is_protected = 4;

        string camera_ap = 11;
        string archive_ap = 12;

        string group_id = 21; // guid of group of comments
        primitive.Rectangle boundary = 22;
        primitive.TimeRange range = 23;
        primitive.Geometry geometry = 24; 
        string message = 25;
    }
	
    message BookmarkToAdd
    {
        BookmarkInternal bookmark_internal = 1;
        string alert_id = 2;
    }
	
    message BookmarkToChange
    {
        BookmarkInternal bookmark_internal = 1;
        string book_mark_guid = 2;
    }
	
    repeated BookmarkToAdd added = 1;
    repeated BookmarkToChange changed = 2;
}

message ChangeBookmarksResponse
{
}

message RecInfoRequest
{
    string access_point = 1;
    bool update_cache = 2;
}

message RecInfoResponse
{
    message RecordingInfo
    {
        int64 system_size = 1;
        int64 recording_size = 2;
        int64 recording_rate = 3;
        int64 capacity = 4;
        int64 last_update = 5;
    }

    RecordingInfo recording_info = 1;
}

// TODO: translate comments
enum EStartPosition
{
    START_POSITION_AT_KEY_FRAME = 0;           ///< на ключевом кадре для заданной позиции
    START_POSITION_EXACTLY = 1;                ///< на ключевом кадре для заданной позиции, ранние семплы помечаются как preroll
    START_POSITION_ONE_FRAME_BACK = 2;         ///< на ключевом кадре для позиции на один кадр назад, ранние семплы помечаются как preroll
    START_POSITION_NEAREST_KEY_FRAME = 3;      ///< на ближайшем ключевом кадре после заданной позиции
    START_POSITION_AT_KEY_FRAME_OR_AT_EOS = 4; ///< кто ближе для заданной позиции - на ключевом кадре или на конце потока
};

/// Priority of reading from the storage.
/// Note that some embedded storages does not support multiply connection.
/// So the priotity should be used to decide which of connections should be established.
enum EEndpointReaderPriority
{
    ERP_Low = 0;
    ERP_Mid = 1;
    ERP_High = 2;
};

message CreateReaderEndpointRequest
{
    // Storage source access_point
    string access_point = 1;
    
    // media timestamp to start at
    string begin_time = 2;
    
    // adjusts media reading policy at begin_time
    EStartPosition start_pos_flag = 3;
    
    // TODO: add comment
    bool is_realtime = 4;
    
    // NMMSS::EPlayModeFlags bit mask
    // Leave empty (0) if in doubts.
    // TODO: add to intefaces
    int32 mode = 5;
    
    // used priority
    EEndpointReaderPriority priority = 6;
};

message CreateReaderEndpointResponse
{
    media.EndpointRef endpoint = 1;
}


message SeekRequest
{
    media.EndpointRef endpoint = 1;
    
    string begin_time = 2;
    
    // adjusts media reading policy at begin_time
    EStartPosition start_pos_flag = 3;
    
    // NMMSS::EPlayModeFlags bit mask
    // Leave empty (0) if in doubts.
    // TODO: add to intefaces
    int32 mode = 4;
    
    // Adjust media samples session id after locating at position.
    uint32 session_id = 5;
}

message SeekResponse
{
}


// TODO: translate to english
/// Получение истории хранилища для данного источника.
/// Интервалы выдаются (в количестве не более maxCount) начиная с beginTime и до endTime.
/// @param minGap задает минимальный временнОй промежуток между интервалами,
/// при котором два интервала считаются различными. Соседние интервалы,
/// разделенные меньшим промежутком, будут "склеены" в один интервал.
/// Этот параметр задается в миллисекундах. Если он равен 0, процедура "склеивания"
/// интервалов применяться не будет.
/// Если кол-ов интервалов в хранилище больше чем @param maxCount, то интервалы с 
/// самыми маленькими промежутками между собой будут объединены в более крупные.

message GetHistoryRequest
{
    string access_point = 1;
    string begin_time = 2;
    string end_time = 3;
    uint32 max_count = 4;
    uint32 min_gap = 5;
}

// TODO: translate
/// Массив с результатом передается через @param intervals, где
/// каждая граница интервала определена через кол-во миллисекунд прошедших с 'universal_time 1900-01-01'
message GetHistoryResponse
{
    message Interval
    {
        int64 begin_time = 1;
        int64 end_time = 2;
    }
    repeated Interval intervals = 1;
}

// TODO: translate
// Получить размер дискового пространства (в байтах) и продолжительность (в секундах) проигрывания 
// занимаемого в указанном временном интервале
message GetSizeRequest
{
    string access_point = 1;
    string begin_time = 2;
    string end_time = 3;
}

message GetSizeResponse
{
    // disk usage in bytes
    int64 size = 1;
    // stored playback duration in seconds
    int64 duration = 2;
}


service ArchiveService
{
    rpc ChangeBookmarks(ChangeBookmarksRequest) returns (ChangeBookmarksResponse) {
        option (google.api.http) = {
            post: "/v1/archive/action:makeUserComment"
            body: "*"
        };
    }

    rpc GetRecordingInfo(RecInfoRequest) returns (RecInfoResponse) {
        option (google.api.http) = {
            get: "/v1/archive/recordingInfo"
        };
    }

    rpc CreateReaderEndpoint(CreateReaderEndpointRequest) returns (CreateReaderEndpointResponse) {
           option (google.api.http) = {
            post: "/v1/archive/action:createEndpoint"
            body: "*"
        };
    }

    rpc GetHistory(GetHistoryRequest) returns (GetHistoryResponse) {
        option (google.api.http) = {
            get: "/v1/archive/history"
        };
    }

    rpc GetSize(GetSizeRequest) returns (GetSizeResponse) {
        option (google.api.http) = {
            get: "/v1/archive/size"
        };
    }

    rpc Seek(SeekRequest) returns (SeekResponse) {
          option (google.api.http) = {
            post: "/v1/archive/action:seek"
            body: "*"
        };
    }

}
