syntax = "proto3";
package axxonsoft.bl.acfa;

option go_package = "bitbucket.org/Axxonsoft/bl/acfa";

import "google/api/annotations.proto";
import "axxonsoft/integration/Properties.proto";
import "axxonsoft/bl/events/Events.proto";

message TestMessageRequest
{
    string method = 1;
    string access_point = 2;
    string uid = 3;
    string data = 4;
}

message TestMessageResponse
{
    string response = 1;
}

enum EUnitStatus
{
    STATUS_UNSPECIFIED = 0;
    STATUS_ACTIVE = 1;
    STATUS_INACTIVE = 2;
}

enum EUnitDisplayMode
{
    VM_FULL = 0;
    VM_WITH_SETTINGS = 1; //to get UD with current properties.
    VM_WITH_SUB_UNITS = 2; //to get UD with sub units.
    VM_ONLY_ROOT = 3; //to get UD without sub units and settings.
}

// This message can be used to configure and obtain a description of the root units.
message RootUnit
{
    // Unique numeric device id provided by client.
    string id = 1;

    // Unique unit id. Need for synchronize with driver (empty when RootUnit is creating).
    string unit_uid = 2;

    // User specified or properly localized name for unit.
	string display_name = 3;

    // Unit type: can be generic, videosource, audiosource, ptz and so on. 
    string type = 4;

    // access point to the CORBA object i.e. root-device, where 'units' are child objects of this device.
    // (empty when RootUnit is creating).
    string access_point = 5;

    // Expected format ip:port:login:password.
    // Is necessary when creating the root-object to connect to the grpc driver.
    string driver_connection_info = 6;

    // Device model. Need for creating.
    integration.Model model = 7;

    // Description of properties of root unit like connection info to the device, etc.
    repeated integration.Value settings = 8;

    // Units that are stored in the CORBA-object by 'access_point'. (may be empty when creating)
    repeated integration.Unit units = 9;

    // Unit activations status
    EUnitStatus status = 10;
}

message SetLanguageRequest
{
    string language = 1;
}

message SetLanguageResponse
{

}

message ObjectsRequest
{
    message Object
    {
        // Unique identifier for the entire server.
        repeated string unit_uids = 1;
        // access point to the CORBA object where contains 'unit_uids' objects.
        string access_point = 2;
    }

    repeated Object objects = 1;
}

message ListUnitsRequest
{
    // Unique numeric device id provided by client.
    repeated string id = 1;

    // Use display_mode to get stripped unit description (UD)
    // If it is omitted (or equal to zero) full UD will be returned.
    // display_mode it is a mask, so the modes can be combined.
    EUnitDisplayMode display_mode = 3;
}

message ListUnitsResponse
{
    repeated RootUnit units = 1;
    // Temporary unavailable objects
    repeated string unreachable_objects = 2;
    // Objects does not exists
    repeated string not_found_objects = 3;
}

enum EChildUnitMode
{
    CU_ADD = 0; //To add new child units.
    CU_CHANGE = 1; //To change exists child units.
}

message TaggedChanges
{
    message Settings
    {
        // Unique unit id. Need for synchronize with driver.(empty if changing exist child units)
        string uid = 1;

        // To change settings for unit by 'uid'
        repeated integration.Value settings = 2;
    }

    message Children
    {
        // Unique unit id. Need for synchronize with driver.(empty if changing exist child units)
        string parent_uid = 1;

        // To add new or change exists child units
        repeated integration.Unit units = 2;
    }

    // access point to the CORBA object i.e. root-device, where 'units' are child objects of this device.
    string access_point = 1;

    // Unique numeric device id provided by client.
    string root_id = 2;

    string etag = 3;

    oneof ConfigurateUnit {
        // To change settings
        Settings settings = 10;

        // To add new child units
        Children children = 11;
    }
}

message TaggedRemoved
{
    // Unique numeric device id provided by client.
    string root_id = 1;

    // access point to the CORBA object i.e. root-device, where 'units' are child objects of this device.
    string access_point = 2;

    string etag = 3;

    repeated string removed_unit_uids = 4;
}

message ChangeUnitsRequest
{
    repeated RootUnit added = 1;
    repeated TaggedChanges changed = 2;
    repeated TaggedRemoved removed = 3;
}

message ChangeUnitsResponse
{
    // Returns unit uids of actually added, changed, removed or failed objects
    repeated string failed = 1;

    repeated string added = 2;
    repeated string changed = 3;
    repeated string removed = 4;
}

message GetConfigRequest
{
    // Unique numeric device id provided by client.
    string id = 1;

    // Unique identifier for the entire server. Need to synchronize with driver.
    string unit_uid = 2;

    // access point to the CORBA object.
    string access_point = 3;

    string etag = 4;
}

message GetConfigResponse
{
    repeated integration.Unit units = 1;
    repeated integration.Value settings = 2;
}

message GetSupportedDevicesRequest
{

}

message GetSupportedDevicesResponse
{
    repeated integration.Model models = 1;
} 

message Actions
{
    string unit_uid = 1;
    repeated integration.Action actions = 2;
}

message AcquireMapActionsResponse
{
    repeated Actions actions = 1;
}

message PerformActionRequest
{
    string unit_uid = 1;
    string action_id = 2;
    repeated integration.Value params = 3;

    // access point to the CORBA object where contains 'unit_uid' object.
    string access_point = 4;
}

message PerformActionResponse
{
    // action output params
    repeated integration.Value params = 1;
    // not empty for Autodetect and Download Config actions
    repeated integration.Unit units = 2;
}

message GetFileRequest
{
    string unit_uid = 1;
    // usually fileID is relative image file name, from image property of visualisation
    string fileID = 2;

    // access point to the CORBA object where contains 'unit_uid' object.
    string access_point = 3;
}

message GetFileResponse
{
    bytes fileChunk = 1;
}

message Visualizations
{
    string unit_uid = 1;
    repeated integration.Visualisation vis = 2;

    // access point to the CORBA object where contains 'unit_uid' object.
    string access_point = 3;
}

message GetVisualizationsResponse
{
    repeated Visualizations visualisations = 1;
}

message EventsAndStates
{
    string unit_uid = 1;

    repeated integration.State states = 2;
    repeated integration.Event events = 3;

    // access point to the CORBA object where contains 'unit_uid' object.
    string access_point = 4;
}

message GetEventsAndStatesResponse
{
    repeated EventsAndStates es = 1;
}

message AcquireSettingsRequest
{
    string unit_uid = 1;

    // access point to the CORBA object where contains 'unit_uid' object.
    string access_point = 2;
}

message AcquireSettingsResponse
{
    repeated integration.Value settings = 1;
    repeated integration.Unit units = 2;
}

message ReceiveEventRequest
{
    events.ACFAEvent event = 1;
}

message ReceiveEventResponse
{
    bool status = 1;
}

service ACFAService
{
    rpc SetLanguage(SetLanguageRequest) returns (SetLanguageResponse);

    rpc ListUnits(ListUnitsRequest) returns (ListUnitsResponse);

    rpc ChangeUnits(ChangeUnitsRequest) returns (ChangeUnitsResponse);

    rpc GetConfig(GetConfigRequest) returns (GetConfigResponse);

    // Returns list of supported by module devices
    rpc GetSupportedDevices(GetSupportedDevicesRequest) returns (GetSupportedDevicesResponse);

    // Return avalaible for object actions, that perfromed from then map, i.e. do same
    // for as AcquireSettings but for map actions list.
    rpc AcquireMapActions(ObjectsRequest) returns (AcquireMapActionsResponse); 

    // Perform object action. Action can be one of returned by GetMapActionsList or condfiguration action,
    // that returned together with settings by AcquireSettings
    rpc PerformAction(PerformActionRequest) returns (PerformActionResponse);

    // GetFile download file from driver. This can be image file, mentioned in visualisation
    rpc GetFile(GetFileRequest) returns (stream GetFileResponse);
    
    // GetVisualisations returns list of avalaible for object visualions
    rpc GetVisualizations(ObjectsRequest) returns (GetVisualizationsResponse);
    
    // GetEvents returns list of all object events. Required to have names of events/states and names of its parameters
    rpc GetEventsAndStates(ObjectsRequest) returns (GetEventsAndStatesResponse);
    
    // Get current object settings and set of possible children
    rpc AcquireSettings(AcquireSettingsRequest) returns (AcquireSettingsResponse);
    
    rpc ReceiveEvent(ReceiveEventRequest) returns (ReceiveEventResponse);
    ///////////////////////////////
    //Terminate close application//
    rpc Test(TestMessageRequest) returns (TestMessageResponse); 
}
