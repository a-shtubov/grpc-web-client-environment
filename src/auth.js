import { AuthenticateRequest } from "./axxonsoft/bl/auth/Authentication_pb";
import { AuthenticationServicePromiseClient } from "./axxonsoft/bl/auth/Authentication_grpc_web_pb";
import { API_PREFIX } from "./constants";

const authService = new AuthenticationServicePromiseClient(API_PREFIX);

let metadata = null;

async function authenticate() {
  const request = new AuthenticateRequest();

  request.setUserName(process.env.AUTH_LOGIN);
  request.setPassword(process.env.AUTH_PASSWORD);

  const authResponse = await authService.authenticate(request);

  metadata = {
    [authResponse.getTokenName()]: authResponse.getTokenValue(),
  };

  return metadata;
}

export async function getAuthMetadata() {
  if (metadata) {
    return metadata;
  }

  return authenticate();
}
